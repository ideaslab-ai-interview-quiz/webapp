# Quiz description

## Introduction
- Develop a simple web application with YOLOv8 and has the features below:
    1. Uploading images.
    2. Running a YOLOv8 model (any of official pre-trained models is sufficient).
    3. Visualizing the results by showing bounding boxes with names or the pose skeleton.
    4. Saving the output to a JSON file.
    5. It's ok to run this web app only on localhost.

## Technical requirements
1. Ensure clear separation between the backend and frontend.
2. Use FastAPI for backend development.
3. Use Streamlit for frontend development. (hint: using requests to make API calls)
4. Use the Pydantic data model in the 'data.py' file to validate prediction and produce JSON file.

## Optional requirements
1. Please provide a simple documentation for this project and it’s recommended to upload the project to GitHub and write the documentation in the README file.

# Reference
1. YOLOv8: https://docs.ultralytics.com/usage/python/#predict
2. For detailed examples, please check out: https://drive.google.com/drive/folders/1_A8jacyyzWYMLzXOGHigOso9gsIKVYC2?usp=sharing
    - demo_image.jpeg: input image
    - saved_output.json: the output format
    - demo_video: shows how the app should look like